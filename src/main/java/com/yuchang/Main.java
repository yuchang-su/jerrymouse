package com.yuchang;

import com.yuchang.connector.HttpConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author yuchang.su
 * @description ${description}
 * @date 2024/1/18 19:31:01
 */
public class Main {
    static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        try (HttpConnector connector = new HttpConnector()) {
            for (; ; ) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    break;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        logger.info("jerrymouse http server was shutdown.");
    }
}