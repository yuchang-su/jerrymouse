package com.yuchang.connector;

import java.net.URI;

/**
 * @author yuchang.su
 * @description HttpRequest接口，负责接收请求
 * @date 2024/1/18 20:00:17
 */
public interface HttpExchangeRequest {

    /**
     * 获取请求方法
     * @return 方法名
     */
    String getRequestMethod();

    /**
     * 获取请求URI
     * @return {@link URI}
     */
    URI getRequestURI();
}
