package com.yuchang.connector;

import com.sun.net.httpserver.Headers;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author yuchang.su
 * @description HttpResponse接口，负责处理请求
 * @date 2024/1/18 20:02:34
 */
public interface HttpExchangeResponse {

    /**
     * 获取请求头
     * @return {@link Headers}
     */
    Headers getResponseHeaders();

    /**
     *
     * @param rCode 响应码
     * @param responseLength 响应
     * @throws IOException IO异常
     */
    void sendResponseHeaders(int rCode, long responseLength) throws IOException;

    /**
     * 获取输出流
     * @return {@link OutputStream}
     */
    OutputStream getResponseBody();
}
