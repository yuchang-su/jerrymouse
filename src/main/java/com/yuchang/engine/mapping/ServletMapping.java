package com.yuchang.engine.mapping;

import jakarta.servlet.Servlet;

/**
 * @author yurisu
 * @description 一个Servlet实例
 * @since 2024/1/21 21:48
 */
public class ServletMapping extends AbstractMapping {

    public final Servlet servlet;

    public ServletMapping(String urlPattern, Servlet servlet) {
        super(urlPattern);
        this.servlet = servlet;
    }
}
